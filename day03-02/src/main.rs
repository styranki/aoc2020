fn main() {
    let input = std::fs::read_to_string("input/day03.txt").unwrap();
    let entries: Vec<&str> = input
        .lines()
        .collect();
    let mut result: Vec<usize> = Vec::new();

    let slopes: [(usize, usize); 5] = [(1,1), (3, 1), (5, 1), (7,1), (1,2)];
    for (step_x, step_y) in slopes.iter() {
        let mut num_trees = 0;
        entries.iter().step_by(*step_y).enumerate().for_each(|(i, line)| {
            if(*line).chars().cycle().nth(i*step_x).unwrap() == '#' {
                num_trees += 1;
            }
        });
        result.push(num_trees);
    }

    println!("{:?}", &result);
    println!("{}", result.iter().fold(1, |sum, val| sum * val))
}
