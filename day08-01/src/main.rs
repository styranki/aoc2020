use std::collections::HashSet;

#[derive(Clone, Copy)]
enum Instruction {
    Accumulate(i32),
    Jump(i32),
    Nop(i32),
}

fn main() {
    let input = std::fs::read_to_string("input/day08.txt").unwrap();
    let mut entries: Vec<&str> = input
        .lines()
        .collect();

    let instructions: Vec<Instruction> = entries.iter().map(|x| {
        let words: Vec<&str> = x.split_ascii_whitespace().collect();
        match words[0] {
            "nop" => Instruction::Nop(words[1].parse::<i32>().unwrap()),
            "acc" => Instruction::Accumulate(words[1].parse::<i32>().unwrap()),
            "jmp" => Instruction::Jump(words[1].parse::<i32>().unwrap()),
            _ => panic!("Invalid instruction"),
        }
    }).collect();

    let mut instruction_to_fix = 0;
    let mut accumulator_state: i32 = 0;
    dbg!(instructions.len());

    'outer: loop {
        dbg!(instruction_to_fix);
        let mut visited = HashSet::new();
        let mut nop_or_jmp_seen = 0;
        let mut instruction_pointer = 0;
        accumulator_state = 0;
        
        'inner: loop {
            //dbg!(instruction_pointer);
            //dbg!(accumulator_state);
            if instruction_pointer == instructions.len() {
                break 'outer
            }
            let mut ins = instructions[instruction_pointer];
            if instruction_to_fix == nop_or_jmp_seen {
                ins = match ins {
                    Instruction::Nop(n) => Instruction::Jump(n),
                    Instruction::Jump(n) => Instruction::Nop(n),
                    _ => ins,
                }
            }
            match ins {
                Instruction::Nop(_) => {
                    if visited.insert(instruction_pointer) {
                        nop_or_jmp_seen += 1;
                        instruction_pointer += 1;
                    } else {
                        break
                    }
                },
                Instruction::Jump(n) => {
                    if visited.insert(instruction_pointer) {
                        nop_or_jmp_seen += 1;
                        //dbg!(n);
                        instruction_pointer = (instruction_pointer as i32 +  n) as usize;
                    } else {
                        break
                    }
                },
                Instruction::Accumulate(n) => {
                    if visited.insert(instruction_pointer) {
                        accumulator_state += n;
                        instruction_pointer += 1;
                    } else {
                        break 'inner
                    }
                }

            }

        }
        instruction_to_fix += 1;

    }

    dbg!(accumulator_state);
}
