fn main() {
    let input = std::fs::read_to_string("../input/day01.txt").unwrap();
    let entries: Vec<i32> = input
        .lines()
        .map(|line| line.parse::<i32>().expect("Failed to parse i32"))
        .collect();

    for (i_idx, i) in entries.iter().enumerate() {
        for (j_idx, j) in entries.iter().enumerate() {
            for (k_idx, k) in entries.iter().enumerate() {
                if j_idx == i_idx || i_idx == k_idx || j_idx == k_idx {
                    continue;
                }
                if i + j + k == 2020 {
                    println!("{}, {}, {}", i, j, k);
                    println!("{}", i*j*k);
                }
            }
        }
    }
}
