fn main() {
    let input = std::fs::read_to_string("input/day02.txt").unwrap();
    let entries: Vec<Vec<&str>> = input
        .lines()
        .map(|line| line.split(":").collect())
        .collect();

    let mut num_valid = 0;

    for (rule, password) in entries.iter().map(|x| (x[0], x[1])) {
        let rule: Vec<&str> = rule.split_ascii_whitespace().collect();
        let character = rule[rule.len()-1];
        let range: Vec<usize> = rule[0].split("-").map(|x| x.parse::<usize>().expect("Failed to parse rule range")).collect();

        let count: Vec<usize> = password.match_indices(character).map(|x| x.0).collect();
        match (
            (count.iter().find(|x| **x == range[0])),
            (count.iter().find(|x| **x == range[1]))
        ) {
            (Some(_), None) => num_valid += 1,
            (None, Some(_)) => num_valid += 1,
            _ => {}
        }
    }

    println!("{}", num_valid);
}
