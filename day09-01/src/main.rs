
fn main() {
    let input = std::fs::read_to_string("input/day09.txt").unwrap();
    let mut entries: Vec<&str> = input
        .lines()
        .collect();
    let mut preamble: [usize; 25] = [0; 25];
    let mut invalid_number = 0;

    for i in 0..25 {
        preamble[i] = entries[i].parse::<usize>().unwrap();
    }


    for i in 25..entries.len() {
        let num = entries[i].parse::<usize>().unwrap();
        if pair_sum_exists(num, &preamble) {
            preamble[i % 25] = num;
            continue
        } else {
            invalid_number = num;
            break
        }
    }

    let  entries: Vec<usize> = entries.iter().map(|x| x.parse::<usize>().unwrap()).collect();

    for subarray_len in (2..entries.len() - 1) {
        for subarray_start in (0.. (entries.len() - 1) - subarray_len) {
            let subarray = &entries[subarray_start..(subarray_start + subarray_len)];
            let sum: usize = subarray.iter().sum::<usize>();
            if sum == invalid_number {
                dbg!(subarray.iter().min().unwrap() + subarray.iter().max().unwrap());
            }
        }
    }





}

fn pair_sum_exists(target: usize, list: &[usize]) -> bool {
    for (i_idx, i) in list.iter().enumerate() {
        for (j_idx, j) in list.iter().enumerate() {
            if (j_idx == i_idx) {
                continue;
            }
            if i + j == target {
                return true
            }

        }
    }

    return false
}
