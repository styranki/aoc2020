use std::collections::HashMap;

#[derive(Copy, Clone, Debug)]
enum Instruction {
    Mask(u64, u64, u64),
    Mem(u64, u64)
}
fn parse_instruction(string: &str) -> Instruction {
    let len = string.len();
    if (&string[0..4] == "mask") {
        let mask_str = &string[len-36..len];
        let mut pos_x: u64 = 0;
        let mut pos_1: u64 = 0;
        let mut pos_0: u64 = 0;

        for (i, c) in mask_str.chars().enumerate() {
            let exponent = (35-i) as u32;
            match c {
                'X' => pos_x |= 2_u64.pow(exponent),
                '1' => pos_1 |= 2_u64.pow(exponent),
                '0' => pos_0 |= 2_u64.pow(exponent),
                _ => panic!("Invalid mask char"),
            };
        }

        return Instruction::Mask(pos_x, pos_0, pos_1)
    }
    if (&string[0..3] == "mem") {
        let tmp: Vec<&str> = string.split(" = ").collect();
        let val: u64 = tmp[1].parse::<u64>().expect("Invalid value");
        let addr: u64 = tmp[0][4..tmp[0].len() - 1].parse::<u64>().expect("Invalid address");
        return Instruction::Mem(addr, val)

    }

    dbg!(&string[0..5]);
    panic!("Invalid instruction");
}

fn main() {
    let input = std::fs::read_to_string("input/day14.txt").unwrap();
    let mut instructions: Vec<Instruction> = input
        .lines()
        .map(|x| parse_instruction(x))
        .collect();

    let mut mask: (u64, u64, u64) = (0, 0, 0);
    let mut memory: HashMap<u64, u64> = HashMap::new();

    for ins in instructions.iter() {
        match ins {
            Instruction::Mask(pos_x, pos_0, pos_1) => mask = (*pos_x, *pos_0, *pos_1),
            Instruction::Mem(addr, val) => {
                let base_addr = (addr | mask.2) & !mask.0;
                print_as_bytes(*addr);
                print_as_bytes(base_addr);
                println!(" ");
                let mut addresses = Vec::new();
                let mut bit_slice: [bool; 36] = [false; 36];
                for i in 0..36 {
                    let is_set = (mask.0 & 2_u64.pow(i)) > 0;
                    bit_slice[i as usize] = is_set;
                }

                let set_idx: Vec<usize> = bit_slice.iter().enumerate().filter(|(_, x)| **x == true).map(|(i, _)| i).collect();
                dbg!(&set_idx);
                let num_set: usize = set_idx.len();
                let mut set_state: Vec<u64> = vec![0; num_set];
                let num_combinations = 2_u64.pow(num_set as u32);

                for i in (0..num_combinations) {
                    for j in 0..num_set {
                        let is_set = (i as u64 & 2_u64.pow(j as u32)) > 0;
                        set_state[j] = if is_set {1} else {0};
                    }
                    let combination = set_idx.iter().zip(&set_state).fold(0, |acc, (idx, state)| acc + state * 2_u64.pow(*idx as u32));
                    addresses.push(base_addr | combination);
                    //dbg!(print_as_bytes(combination));

                }

                //print_as_bytes(mask.0);
                for addr in addresses.iter() {
                    print_as_bytes(*addr);
                    memory.insert(*addr, *val);
                }
                println!("---------------------------------------------");
            }
        }
    }

    let sum = memory.iter().fold(0, |acc, (_, &val)| acc+val);
    dbg!(sum);

}

fn print_as_bytes(i: u64) {
    let mut res: String = String::new();

    for j in (0..36) {
        let is_set = (i & 2_u64.pow((35-j) as u32)) > 0 ;
        res.push(if is_set {'1'} else {'0'} )
    }
    println!("{}", &res);
}
