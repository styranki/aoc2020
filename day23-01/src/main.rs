use std::collections::VecDeque;
use itertools::Itertools;

fn main() {
    let input = std::fs::read_to_string("input/day22.txt").unwrap();
    let mut lines: Vec<&str> = input
        //.split(|x| x =='\n')
        .lines()
        .collect();

    let (deck_1_str, deck_2_str) = lines.split(|&x| x == "").collect_tuple().unwrap();

    let mut deck_1: VecDeque<_>  = deck_1_str[1..].iter().map(|x| x.parse::<i32>().unwrap()).collect();
    let mut deck_2: VecDeque<_>  = deck_2_str[1..].iter().map(|x| x.parse::<i32>().unwrap()).collect();


    loop {
        let card_1 = deck_1.pop_front();
        let card_2 = deck_2.pop_front();
        match (card_1, card_2) {
            (Some(card_1), Some(card_2)) => {
                let card_1_won = card_1 > card_2;
                if card_1_won {
                    deck_1.push_back(card_1);
                    deck_1.push_back(card_2);
                } else {
                    deck_2.push_back(card_2);
                    deck_2.push_back(card_1);
                }

            },
            (None, Some(card_2)) => {
                println!("Player 2 won!");
                deck_2.push_front(card_2);
                dbg!(&deck_2);
                let points = (deck_2.iter().rev().enumerate().fold(0, |acc, (idx, val)| {
                    acc + ((idx + 1) as i32 * val)
                }));
                dbg!(points);
                break;
            },
            (Some(card_1), None) => {
                println!("Player 1 won!");
                deck_1.push_front(card_1);
                let points = (deck_1.iter().rev().enumerate().fold(0, |acc, (idx, val)| {
                    acc + ((idx + 1) as i32 * val)
                }));
                dbg!(points);
                break;
            },
            (None, None) => {
                panic!("Impossible state");
            }

        }

    }

}
