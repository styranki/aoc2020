
#[derive(Debug)]
struct Ferry {
    pos: (i32, i32),
    heading: i32,
    waypoint: Vector,
}
impl Ferry {
    fn execute_action(&mut self, action: &Action) {
        match action {
            Action::North(n) => self.waypoint.1 +=  n,
            Action::East(n) => self.waypoint.0 += n,
            Action::South(n) => self.waypoint.1 -=  n,
            Action::West(n) => self.waypoint.0 -= n,
            // % operator actually remainder, not modulo
            Action::Left(n) => {
                match n {
                    90 => self.waypoint = self.waypoint.rotate_90(),
                    180 => self.waypoint = self.waypoint.rotate_90().rotate_90(),
                    270 => self.waypoint = self.waypoint.rotate_90().rotate_90().rotate_90(),
                    _ => panic!("invalid rotation"),
                }
            },
            Action::Right(n) => {
                match n {
                    90 => self.waypoint = self.waypoint.rotate_90().rotate_90().rotate_90(),
                    180 => self.waypoint = self.waypoint.rotate_90().rotate_90(),
                    270 => self.waypoint = self.waypoint.rotate_90(),
                    _ => panic!("invalid rotation"),
                }
            },
            Action::Forward(n) => {
                self.pos = (self.pos.0 + n * self.waypoint.0, self.pos.1 + n * self.waypoint.1);
            }

        }
        dbg!(&self);
    }
}

#[derive(Debug)]
struct Vector(i32, i32);
impl Vector {
    fn rotate_90(&self) -> Self {
        Vector(-self.1, self.0)
    }
}
enum Action {
    North(i32),
    East(i32),
    South(i32),
    West(i32),
    Left(i32),
    Right(i32),
    Forward(i32),
}

fn main() {
    let input = std::fs::read_to_string("input/day12.txt").unwrap();
    let mut entries: Vec<Action> = input
        .lines()
        .map(|x|{
            match (x.chars().next().unwrap()) {
                'N' => Action::North(x[1..x.len()].parse::<i32>().unwrap()),
                'E' => Action::East(x[1..x.len()].parse::<i32>().unwrap()),
                'S' => Action::South(x[1..x.len()].parse::<i32>().unwrap()),
                'W' => Action::West(x[1..x.len()].parse::<i32>().unwrap()),
                'L' => Action::Left(x[1..x.len()].parse::<i32>().unwrap()),
                'R' => Action::Right(x[1..x.len()].parse::<i32>().unwrap()),
                'F' => Action::Forward(x[1..x.len()].parse::<i32>().unwrap()),
                _ => panic!("Invalid action"),
            }

        })
        .collect();

    let mut ship = Ferry {
        pos: (0, 0),
        heading: 0,
        waypoint: Vector(10, 1),
    };

    for action in entries {
        ship.execute_action(&action);
    }

    dbg!(ship.pos.1.abs() + ship.pos.0.abs());

}
