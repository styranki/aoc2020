
fn main() {
    let input = std::fs::read_to_string("input/day10.txt").unwrap();
    let mut entries: Vec<usize> = input
        .lines()
        .map(|x| x.parse::<usize>().unwrap())
        .collect();

    entries.sort();
/*
    let mut prev_jolts = 0;
    let mut jolt_steps = Vec::new();

    for adapter in entries {
        let diff = adapter - prev_jolts;
        match diff {
            1 => jolt_steps.push(1),
            3 => jolt_steps.push(3),
            _ => panic!("Invalid jolt step"),
        }
        prev_jolts = adapter;
    }

    let mut num_combinations: usize = 1;
    let  mut last_3_step = 1;
    dbg!(jolt_steps.len());


    /*for i in (0..jolt_steps.len()) {
        if jolt_steps[i] == 3 || i == jolt_steps.len()-1 {
            dbg!(i);
            let num_step_1 = i - last_3_step;
            dbg!(num_step_1);
            let m = match num_step_1 {
                1 => 1,
                2 => 1,
                3 => 2,
                4 => 4,
                5 => 7,
                _ => panic!("ererererererer"),
            };
            num_combinations *= m;
            last_3_step = i;
        }
       
    }
    dbg!(jolt_steps);
    dbg!(num_combinations);

    */

*/
    let mut dp: std::collections::HashMap<i32, usize> = std::collections::HashMap::new();
    dp.insert(0, 1);
    for i in entries.iter() {
        let ans =
            dp.get(&(*i as i32 - 1)).unwrap_or(&0) +
            dp.get(&(*i as i32 - 2)).unwrap_or(&0) +
            dp.get(&(*i as i32 - 3)).unwrap_or(&0);
            dp.insert(*i as i32, ans);
    }
    dbg!(dp[&(*entries.last().unwrap() as i32)]);

}
fn trib(n: usize) -> usize {
    match n {
        1|2 => 0,
        3 => 1,
        _ => trib(n-1) + trib(n-2) + trib(n-3),
    }

}
/*

10 11 12 13 14 17
10 11 12 14 17
10 11 13 14 17
10 11 14 17

10 12 13 14 17
10 13 14 17

10

*/
