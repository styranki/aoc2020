use std::collections::HashMap;
use std::iter::Iterator;

fn main() {
    let input = std::fs::read_to_string("input/day07.txt").unwrap();
    let mut entries: Vec<&str> = input
        .lines()
        .collect();

    let mut rules: HashMap<String, Vec<(u32, String)>> = HashMap::new();

    for line in entries.iter() {
        let tmp: Vec<String> = line.split("contain ").map(|x| x.to_string()).collect();
        let bag = &tmp[0][0..tmp[0].len() - 6];
        let rule: Vec<String> = tmp[1].split(", ").map(|x| x.to_string()).collect();
        let mut rule_list = Vec::new();
        for rule_str  in rule.iter() {
            if rule_str == "no other bags." {
                break
            }
            let rule_str_split: Vec<&str> = rule_str.split_ascii_whitespace().collect();
            let num = rule_str_split[0].parse::<u32>().unwrap();
            let bag_name = format!("{} {}", rule_str_split[1], rule_str_split[2]);
            rule_list.push((num, bag_name));
        }
        rules.insert(bag.to_string(), rule_list);
    }



    let mut sum = 0;
    for key in rules.keys() {
        if contains_gold_bag(&rules, key) {
            sum += 1;
        }
    }

    dbg!(sum);

    let total = number_of_bags(&rules, "shiny gold");
    dbg!(total);

}

    fn contains_gold_bag(rules: &HashMap<String, Vec<(u32, String)>>, bag: &str) -> bool {
       bag == "shiny gold" || rules[bag].iter().any(|x| contains_gold_bag(rules, &x.1))
    }
    fn number_of_bags(rules: &HashMap<String, Vec<(u32, String)>>, bag: &str) -> u32 {
       1 + rules[bag].iter().map(|(num, name)| num * number_of_bags(rules, &name)).sum::<u32>()
    }
