fn main() {
    let input = std::fs::read_to_string("input/day03.txt").unwrap();
    let entries: Vec<(usize, &str)> = input
        .lines()
        .enumerate()
        .collect();

    let mut num_trees = 0;
    entries.iter().for_each(|(i, line)| {
        let l: Vec<char> = line.chars().collect();
        dbg!(((*line).chars().cycle()).nth(*i*3).unwrap());
        if(((*line).chars().cycle()).nth(*i*3).unwrap() == '#') {
            num_trees += 1;
        }
    });


    println!("{}", num_trees);
}
