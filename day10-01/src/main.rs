
fn main() {
    let input = std::fs::read_to_string("input/day10.txt").unwrap();
    let mut entries: Vec<usize> = input
        .lines()
        .map(|x| x.parse::<usize>().unwrap())
        .collect();

    entries.sort();

    let mut prev_jolts = 0;
    let mut jolt_steps: (usize, usize) = (0, 0);

    for adapter in entries {
        dbg!(adapter);
        let diff = adapter - prev_jolts;
        match diff {
            1 => jolt_steps.0 = jolt_steps.0 + 1,
            3 => jolt_steps.1 = jolt_steps.1 + 1,
            _ => panic!("Invalid jolt step"),
        }
        prev_jolts = adapter;
    }

    jolt_steps.1 += 1;
    dbg!(jolt_steps);
    dbg!(jolt_steps.0 * jolt_steps.1);

}
