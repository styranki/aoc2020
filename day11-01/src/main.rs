use std::collections::HashMap;
use std::iter::Iterator;

#[derive(Clone, Copy, Debug)]
enum Square {
    Floor,
    Empty,
    Occupied,
}

fn main() {
    let input = std::fs::read_to_string("input/day11.txt").unwrap();
    let mut entries: Vec<&str> = input
        .lines()
        .collect();

    let mut seat_layout: Vec<Vec<Square>> = Vec::new();
    let rows = entries.len();
    let columns = entries[0].len();

    for line in entries.iter() {

        let mut row = Vec::new();
        for symbol in line.chars() {
            match symbol {
                '.' => row.push(Square::Floor),
                '#' => row.push(Square::Occupied),
                'L' => row.push(Square::Empty),
                _ => panic!("Unrecognised symbol"),
            }
        }
        seat_layout.push(row);
    }

    loop {
        let prev_state = seat_layout.clone();
        let mut changed = false;

        for i in (0..rows) {
            for j in (0..columns) {
                let adjacent_seats = find_adjacent_seats(i, j, &prev_state);
                let num_occupied: u8 = adjacent_seats.iter().fold(0 as u8, |acc ,x| match x {
                    Some(Square::Occupied) => acc + 1,
                    _ => acc,
                });

                match prev_state.get(i).and_then(|x| x.get(j)) {
                    Some(Square::Empty) => {
                        if num_occupied == 0 {
                            seat_layout[i][j] = Square::Occupied;
                            changed = changed || true;
                        }
                    },
                    Some(Square::Occupied) => {
                        if num_occupied >= 4 {
                            seat_layout[i][j] = Square::Empty;
                            changed = changed || true;
                        }
                    },
                    _ => {},
                }
            }
        }

        if !changed {
            break
        }
    }


    let mut num_occupied = 0;
    for i in (0..rows) {
        for j in (0..columns) {
            match seat_layout.get(i).and_then(|x| x.get(j)) {
                Some(Square::Occupied) => num_occupied += 1,
                _ => {},
            }
        }
    }

   dbg!(num_occupied);

}

fn find_adjacent_seats<'a>(row: usize, column: usize, seat_layout: &'a Vec<Vec<Square>>) -> [Option<&'a Square>; 8] {
    let mut result = [None; 8];


    // Dirty underflow when row or column == 0, should be ok
    result[0] = seat_layout.get(row-1).and_then(|x| x.get(column-1).clone());
    result[1] = seat_layout.get(row-1).and_then(|x| x.get(column).clone());
    result[2] = seat_layout.get(row-1).and_then(|x| x.get(column+1).clone());

    result[3] = seat_layout.get(row).and_then(|x| x.get(column-1).clone());
    result[4] = seat_layout.get(row).and_then(|x| x.get(column+1).clone());

    result[5] = seat_layout.get(row+1).and_then(|x| x.get(column-1).clone());
    result[6] = seat_layout.get(row+1).and_then(|x| x.get(column).clone());
    result[7] = seat_layout.get(row+1).and_then(|x| x.get(column+1).clone());

    result
}
