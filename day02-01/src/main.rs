fn main() {
    let input = std::fs::read_to_string("input/day02.txt").unwrap();
    let entries: Vec<Vec<&str>> = input
        .lines()
        .map(|line| line.split(":").collect())
        .collect();

    let mut num_valid = 0;

    for (rule, password) in entries.iter().map(|x| (x[0], x[1])) {
        let rule: Vec<&str> = rule.split_ascii_whitespace().collect();
        let character = rule[rule.len()-1];
        let range: Vec<usize> = rule[0].split("-").map(|x| x.parse::<usize>().expect("Failed to parse rule range")).collect();

        let count = password.matches(character).count();
        if count >= range[0] && count <= range[1] {
            num_valid += 1;
        }
    }

    println!("{}", num_valid);
}
