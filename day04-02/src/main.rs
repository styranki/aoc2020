use itertools::Itertools;
use std::collections::HashMap;

fn main() {
    let input = std::fs::read_to_string("input/day04.txt").unwrap();
    let entries: Vec<&str> = input
        .lines()
        .collect();
    let mut valid_passports = 0;
    let mut rejected_passports = 0;

    let mandatory_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
    let mut passports: Vec<String> = Vec::new();
    let mut cur_passport = String::new();
    for entry in entries.iter() {
        if *entry == "" {
            passports.push(cur_passport);
            cur_passport = String::new();
        } else {
            cur_passport.push(' ');
            cur_passport.push_str(entry);
        }
    }
    passports.push(cur_passport);

    'passport: for passport in passports.iter() {

        let fields = passport.split_whitespace()
                .flat_map(|p| p.split(':'))
                .tuples()
                .collect::<HashMap<_,_>>();

        for field in mandatory_fields.iter() {
            if !fields.contains_key(field){
                rejected_passports += 1;
                continue 'passport;
            }
        }

        if check_passport_validity(fields) {
            valid_passports += 1;
        } else {
            rejected_passports += 1;
        }
    }

    dbg!(valid_passports);
    dbg!(rejected_passports);
    dbg!(passports);

    fn check_passport_validity(passport: HashMap<&str, &str>) -> bool {
        let byr = passport.get("byr").unwrap().parse::<u32>().unwrap();
        if byr > 2002 || byr < 1920 {
            return false;
        }

        let iyr = passport.get("iyr").unwrap().parse::<u32>().unwrap();
        if iyr > 2020 || iyr < 2010 {
            return false;
        }

        let eyr = passport.get("eyr").unwrap().parse::<u32>().unwrap();
        if eyr > 2030 || eyr < 2020 {
            return false;
        }

        let hgt = passport.get("hgt").unwrap();
        let hgt = hgt.split_at(hgt.len() - 2);
        let hgt = (hgt.0.parse::<u32>().unwrap(), hgt.1);
        match hgt {
            (150..=193, "cm") => {},
            (59..=76, "in") => {},
            _ => {
                return false;
            }

        }

        let hcl = passport.get("hcl").unwrap();
        if hcl.len() != 7 {
            return false;
        }

       
        for (i,c) in hcl.chars().enumerate() {
            if i == 0 && c != '#' {
                return false
            } else {
                if i != 0 && (!c.is_ascii_hexdigit()
                || (c.is_alphabetic() && !c.is_lowercase())) {
                    return false
                }
            }
        }



        let mut valid_ecl = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].iter();
        let ecl = passport.get("ecl").unwrap();
        if valid_ecl.find(|x| x == &ecl).is_none() {
            return false
        }


        let pid = passport.get("pid").unwrap();
        if pid.len() != 9 || pid.parse::<u32>().is_err() {
           return false
        }
        return true
    }
}
